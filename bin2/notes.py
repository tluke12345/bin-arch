#!/bin/python
"""
Browse the notes folder
"""

import subprocess
import sys
from pathlib import Path
from typing import Callable

from pyutils import browser
from pyutils import terminal
from pyutils.browser import menuCall

PATH = Path.home()/"sync/notes"


class ArgParse:
    def __init__(self) -> None:
        self.options = []
        self.help_option = {
            'options': ['-h', '--help'],
            'action': self.print_help,
            'help': 'print help (this screen)'
        }

    def add(self, options: list[str], action: Callable, help: str):
        self.options.append({
            'options': options,
            'action': action,
            'help': help
        })
    
    def parse(self, args: list[str]) -> Callable | None:
        for item in self.options + [self.help_option]:
            item_options = item['options']
            if not args and not item_options:
                return item['action']
            if any(option in args for option in item_options):
                return item['action']


    def print_help(self):
        for item in self.options + [self.help_option]:
            item_options = item['options']
            if item_options:
                option = ', '.join(item_options)
            else:
                option = '[none]'
            print(f'{option:20}{item["help"]}')
    

def notes_menu():
    b = browser.PathBrowser(PATH)
    b.menuCall = menuCall.dmenuGridMenuCall
    result = b.go()
    if result:
        terminal.editor(result)


def git_pull_notes():
    process = subprocess.Popen(['git', 'pull'], cwd=PATH, text=True)
    process.communicate()


if __name__ == "__main__":

    parser = ArgParse()
    parser.add(
        [],
        notes_menu,
        'display dmenu notes browser'
    )
    parser.add(
        ['-u', '--update'],
        git_pull_notes,
        'git: pull changes from notes repository'
    )
    action = parser.parse(sys.argv[1:])
    if action:
        action()
